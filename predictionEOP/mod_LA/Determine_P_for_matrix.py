from heapq import nsmallest
import numpy as np
import time

start_time = time.time()


class DetermineP:
    def __init__(self, real_range, interval, p_start, tau):
        self.real_range = real_range
        #self.length_pred = length_pred
        self.interval = interval
        self.N = len(real_range)
        self.tau = tau
        self.p = p_start

    def matrix_delay(self, p):
        delay_matrix = np.zeros((self.N - self.tau * (p - 1), p))
        for k in range(0, self.N - self.tau * (p - 1)):
            for i in range(0, p, ):
                delay_matrix[k, i] = self.real_range[k + self.tau * i]
        return delay_matrix

    def fnn(self):
        fnn = 0
        p_max = self.interval / 5
        while self.p < p_max-2:
            delay_matrix = self.matrix_delay(self.p)
            dist = [np.linalg.norm(delay_matrix[-1, :] - delay_matrix[i, :]) for i
                    in range(self.N - self.tau * (self.p))]
            matrix_neighb = np.array(
                [delay_matrix[i, :] for i in map(dist.index, nsmallest(2, dist))])
            fnn1 = fnn
            fnn = np.linalg.norm(matrix_neighb[1, :] - matrix_neighb[0, :])
            fn2 = abs(matrix_neighb[1, 0] - matrix_neighb[0, 0]) \
                  / np.linalg.norm(matrix_neighb[1, :] - matrix_neighb[0, :])
            if fnn - fnn1 < 0.005:
                break
            self.p += 2
        return self.p
