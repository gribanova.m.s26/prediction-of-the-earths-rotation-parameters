# Получение параметров из файла
import dateutil.parser
from astropy.time import Time


def read_config(Path=''):
    import configparser
    config = configparser.ConfigParser()
    config.read(Path + "config.ini", encoding="utf-8-sig")

    Input = {'Name_final_range': config["input"]["name_final"],
             'Name_pred_range': config["input"]["name_pred"],
             'Start': config["input"]["time_start"],
             'Start_mjd': int((Time(dateutil.parser.parse(config["input"]["time_start"]))).mjd),
             'End': config["input"]["time_end"],
             'End_mjd': int((Time(dateutil.parser.parse(config["input"]["time_end"]))).mjd),
             'Max_length_pr': int(config["input"]["max_length_pr"]),

             'choose_mjd': (config["input"]["mjd"]).split(),
             'choose_eop': (config["input"]["EOP"]).split(),
             'flag_pred': config["input"]["flag_pred"],
             'Step_test': int(config["input"]["step"]),

             'Interval_for_prediction': int(365 * float(config["pred_range"]["interval_for_prediction"])),
             'Method': config["pred_range"]["method"],
             'Rank_LA': int(config["pred_range"]["rank"]),
             'flag_p': config["pred_range"]["flag_p"],
             'P_for_matrix_delay': int(config["pred_range"]["p"]),
             'P_for_short': int(config["pred_range"]["p_short"]),
             'P_for_long': int(config["pred_range"]["p_long"]),
             'Tau': int(config["pred_range"]["tau"]),
             'mod_get_pred': (config["pred_range"]["mod"]),

             'mod_UTC_UT1R': (config["pred_range"]["mod_utc"]),
             'tabl_tides': (config["pred_range"]["tabl_tides"]),
             'tabl_tai': (config["pred_range"]["tabl_tai"]),

             'tabl_rms': config["output"]["tabl_rms"],
             'tabl_pred': config["output"]["tabl_pred"],

             'Name_iaa_range': (config["interpol"]["pred_range"]),
             'Interval_for_rapid_prediction': int(config["interpol"]["days_pred"]),
             'Interval_for_replace': int(config["interpol"]["days_repl"]),
             }
    return Input
