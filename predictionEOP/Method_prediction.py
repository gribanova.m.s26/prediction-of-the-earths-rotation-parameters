import predictionEOP.mod_LA.LA_prediction as LA


def choose_method(input, Name_pred_range, Start_pred_range, Max_length_pr):
    if 'LA+comb' in input['mod_get_pred']:
        return LA.LA_comb(input, Name_pred_range, Start_pred_range, Max_length_pr)
    if 'LA+repl_pred_iaa' in input['mod_get_pred']:
        return LA.LA_repl_pred_iaa(input, Name_pred_range, Start_pred_range, Max_length_pr)
    if 'LA+repl_ref_iaa' in input['mod_get_pred']:
        return LA.LA_repl_ref_iaa(input, Name_pred_range, Start_pred_range, Max_length_pr)
    if 'ML' in input['mod_get_pred']:
        pass
    if 'LA' in input['mod_get_pred']:
        return LA.LA_prediction(input, Name_pred_range, Start_pred_range, Max_length_pr)

