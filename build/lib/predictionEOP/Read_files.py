# Функция для чтения рядов с опорным интервалом из файлов
# Входные данные: имя файла, дата начала и конца интервала, имена нужных пвз для прогеноза,
# имя временной шкалы (MJD)
import pandas as pd


class ReadFile:

    def __init__(self, name, start, end, choose_eop, mjd):
        self.name = name
        self.start = start
        self.end = end
        self.choose_eop = choose_eop
        self.mjd = mjd

    # поиск строки с начальной и конечной датой интервала
    def choose_date(self, df):
        return df[(df[df[self.mjd[0]] >= int(self.start)].index[0]):(df[df[self.mjd[0]] == int(self.end)].index[0]) + 1].reset_index(
            drop=True)

    # чтение данных из разных типов файлов
    def type_of_file(self):
        # bull !!!!
        if 'csv' in self.name:
            df = (pd.read_csv(self.name, sep=';'))[self.mjd + self.choose_eop]

        elif 'eopc04' in self.name:
            numb_col = {'MJD': 3, 'x_pole': 4, 'y_pole': 5, 'UT1-UTC': 6}
            df = (pd.read_table(self.name, sep='\s+', header=None, skiprows=15))[[numb_col[i] for i in (self.mjd + self.choose_eop)]]
            df.columns = self.mjd + self.choose_eop

        elif 'veop' in self.name:
            numb_col = {'MJD': 0, 'x_pole': 1, 'y_pole': 2, 'UT1-UTC': 3}
            df = (pd.read_table(self.name, sep='\s+', header=None, skiprows=5))[[numb_col[i] for i in (self.mjd + self.choose_eop)]]
            df.columns = self.mjd + self.choose_eop
            df = df[
                ((int(self.start)+1 < df['MJD']) & (df['MJD'] <= int(self.end)+1))]
            return df

        elif 'finals' in self.name or 'prediction' in self.name:

            numb_col = {'date': (0, 7), 'MJD': (7, 16), 'I': (16, 18), 'x_pole': (18, 28), 'y_pole': (37, 47),
                        'I2': (56, 58), 'UT1-UTC': (58, 69)}
            df = pd.read_fwf(self.name, colspecs=[numb_col[i] for i in (numb_col.keys())],
                             names=['date', 'MJD', 'I', 'x_pole', 'y_pole', 'I2', 'UT1-UTC'])
        else:
            df = 0
        df = self.choose_date(df)
        return df
