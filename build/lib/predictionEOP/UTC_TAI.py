# Функция для перевода UT1-UTC в TAI-UT1 и опционально UT1 - UT1R
# Входные данные: MJD list, UTC list
# Также нужны файлы EARTH ROTATION VARIATIONS DUE TO ZONAL TIDES.txt и TAI_UTC.DAT


import numpy as np
import pandas as pd


# чтение файла TAI-UTC
def read_tai(mjd, tabl_tai):
    t = pd.read_table(tabl_tai, sep='\s+', header=None)
    t['MJD_start'] = t[0]
    t["MJD_end"] = t['MJD_start'][1::].to_list() + [99999]
    return int(t[t.eval('(MJD_start <= {}) & ({} < MJD_end)'.format(mjd, mjd))][1])


'''def read_tai(tabltai):
    t = pd.read_table(tabltai, sep='\s+', header=None)
    mjd, dt0 = t[0].tolist(), t[1].tolist()
    return mjd, dt0
'''


# чтение файла с коэффициентами Деланэ
def read_tides(tabltides):
    t = pd.read_table(tabltides, sep='\s+', skiprows=6, nrows=47, header=None)
    A_ut1r = t[6].tolist()
    coeff_delan_arg = [t[i].tolist() for i in range(1, 6)]
    return A_ut1r, coeff_delan_arg


def mjd_to_jc(mjd):
    jc = (mjd - 51544.5) / 36525.0
    return jc


def dmod2pi(x):
    d = x % (2 * np.pi)
    if x >= 0 or d == 0:
        return d
    else:
        return d + abs(2 * np.pi)


# Класс перевода UT1-UTC в TAI-UT1 и опционально UT1 - UT1R.  TAI-UT1 = TAI-UTC - UT1-UTC
class TransUT1R:
    def __init__(self, MJD, UTC, tabl_tides, tabl_tai, mod_UTC_UT1R):
        self.MJD = MJD
        self.UTC = UTC
        self.tabl_tides = tabl_tides
        self.tabl_tai = tabl_tai
        self.mod_UTC_UT1R = mod_UTC_UT1R

    # UT1 -> UT1R ?
    def type_transform(self):
        if self.mod_UTC_UT1R == 'yes':
            return self.ut1_ut1r()
        else:
            return self.trans_tai_utc()

    # Вычисление TAI - UT1
    def trans_tai_utc(self):
        return [read_tai(self.MJD[i], self.tabl_tai) - self.UTC[i] for i in range(len(self.MJD))]


    # Вычисление модификации TAI - UT1R
    def ut1_ut1r(self):
        tai_ut1r = []
        tai_ut1 = self.trans_tai_utc()
        for i in range(len(self.UTC)):
            tai_ut1r.append(tai_ut1[i] - self.tidal_variat_earth_rotation(self.MJD[i], ))
        return tai_ut1r

    # Вычисление поправки UT1 - UT1R
    def tidal_variat_earth_rotation(self, mjd, ):
        A_ut1r, coeff_delan_arg = read_tides(self.tabl_tides)
        delan_arg = self.fundament_argum(mjd)
        dUT = 0
        for k in range(len(A_ut1r)):
            # a1 l + a2 l' + a3 D + a4 F + a5 Ω
            q = sum([coeff_delan_arg[k][i] * delan_arg[i] for i in range(5)])
            # UT1 - UT1R = Σ A * sin (q)
            dUT = dUT + A_ut1r[k] * np.sin(q)
        return dUT

    # Расчет Delaunay arguments
    def fundament_argum(self, mjd):
        const0 = [134.96340251,
                  357.52910918,
                  93.27209062,
                  297.85019547,
                  125.04455501, ]

        const1 = [1717915923.217800,
                  129596581.048100,
                  1739527262.847800,
                  1602961601.209000,
                  -6962890.543100, ]

        const2 = [31.879200,
                  -0.553200,
                  -12.751200,
                  -6.370600,
                  7.472200, ]

        const3 = [0.05163500,
                  0.00013600,
                  -0.00103700,
                  0.00659300,
                  0.00770200, ]

        const4 = [-0.0002447000,
                  -0.0000114900,
                  0.0000041700,
                  - 0.0000316900,
                  -0.0000593900, ]

        Fund_arg = np.zeros(5)
        jc = mjd_to_jc(mjd)
        for i in range(5):
            Fund_arg[i] = dmod2pi(np.radians(
                ((((const4[i] * jc + const3[i]) * jc + const2[i]) * jc + const1[i]) * jc) / 3600 + const0[i]))
        return Fund_arg
