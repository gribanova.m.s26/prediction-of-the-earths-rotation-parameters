# Функция локальной аппроксимации, 3 варианта: итеаритвный, прямой, прямой с пересчетом
# Входные данные: ряд, параметр многомерного представления - Р,
# Временной сдвиг между компонентами матрицы задержки - tau,
# метод и длина прогноза в днях.


from heapq import nsmallest
import numpy as np
import time

start_time = time.time()


class LAmethod:
    def __init__(self, real_range, p, tau, method, length_pred):
        self.real_range = real_range
        self.p = p
        self.numb_neighb = int(3 * (self.p + 1))
        self.tau = tau
        self.N = len(real_range)
        self.method = method
        self.length_pred = length_pred

# метод прогнозирования
    def methods_prediction(self, rank=1):
        if 'iterat' in self.method:
            return self.iterat()
        elif 'direct' in self.method:
            return self.direct(rank)
        elif 'count' in self.method:
            return self.iterat_w_count()


# матрица задержек (размерность p * (N - p +1)
    def matrix_delay(self):
        delay_matrix = np.zeros((self.N - self.tau * (self.p - 1), self.p))
        for k in range(self.N - self.tau * (self.p - 1)):
            for i in range(self.p,):
                delay_matrix[k, i] = self.real_range[k + self.tau*i]
        return delay_matrix

# матрица соседей и стартовый вектор
    def near_neighbor(self):
        delay_matrix = self.matrix_delay()
        dist = [np.linalg.norm(delay_matrix[-1, :] - delay_matrix[i, :]) for i
                in range(self.N - self.tau * self.p)]
        matrix_neighb = np.array(
            [delay_matrix[i, :] for i in map(dist.index, nsmallest(self.numb_neighb, dist))])
        return matrix_neighb, delay_matrix[-1, :]

    # усреднение по формуле
    def average(self, var):
        return (np.ones(self.numb_neighb) / self.numb_neighb).dot(var)

    def x_ix(self, x):
        return x - np.ones(self.numb_neighb).reshape(self.numb_neighb, 1) * self.average(x)

    # Вычисление параметра а
    def parameters(self, x, y):
        X_IX = self.x_ix(x)
        return np.linalg.inv(np.transpose(X_IX).dot(X_IX)).dot(np.transpose(X_IX)) \
            .dot(y - np.ones(self.numb_neighb).dot(self.average(y)))

    # вектор эволюции соседей на к шагов вперед
    def vect_evol_neighbor(self, array, x, k):
        index_vect_evol = []
        for i in range(self.numb_neighb):
            index_vect_evol.append(array.index(x[i, -1]))
        return np.array([array[i + k] for i in index_vect_evol])

    # итеративный
    def iterat(self):
        X, start_vect = self.near_neighbor()
        Y = self.vect_evol_neighbor(self.real_range, X, 1 * self.tau)
        aver_Y = self.average(Y)
        aver_X = self.average(X)
        a = self.parameters(X, Y)
        x_t = []
        while self.length_pred > 0:
            x_1 = aver_Y + (start_vect - aver_X).dot(a)
            x_t.append(x_1)
            start_vect = np.append(start_vect[1::], x_1)
            self.length_pred -= 1
        return x_t

    # прямой
    def direct(self, rank):
        var_range = self.real_range
        X, start_vect = self.near_neighbor()
        for k in range(1, self.length_pred + 1):
            Y = self.vect_evol_neighbor(var_range, X, k)
            aver_Y = self.average(Y)
            if rank == 1:
                aver_X = self.average(X)
                a = self.parameters(X, Y)
                var_range.append(aver_Y + (start_vect - aver_X).dot(a))
            else:
                var_range.append(aver_Y)
        return var_range[-self.length_pred::]

    # итеративный с пересчетом
    def iterat_w_count(self):
        numb_pred = self.length_pred
        var_range = self.real_range
        while numb_pred > 0:
            X, start_vect = self.near_neighbor()
            Y = self.vect_evol_neighbor(var_range, X, 1)
            aver_Y = self.average(Y)
            aver_X = self.average(X)
            a = self.parameters(X, Y)
            var_range.append(aver_Y + (start_vect - aver_X).dot(a))
            self.N = len(var_range)
            numb_pred -= 1
        return var_range[-self.length_pred::]