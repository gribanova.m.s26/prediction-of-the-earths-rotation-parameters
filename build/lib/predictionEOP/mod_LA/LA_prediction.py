import sys


def LA_prediction(input, Name_pred_range, Start_pred_range, Max_length_pr):
    from predictionEOP import Read_files, UTC_TAI
    from predictionEOP.mod_LA import Local_aproximation
    import pandas as pd

    # Чтение основного ряда
    Interv_pred_range = Read_files.ReadFile(Name_pred_range, str(Start_pred_range - input['Interval_for_prediction']),
                                            str(Start_pred_range - 1), input['choose_eop'],
                                            input['choose_mjd']).type_of_file()

    # Перевод UT1-UTC в UT1-TAI
    if 'UT1-UTC' in input['choose_eop']:
        Interv_pred_range['UT1-UTC'] = \
            UTC_TAI.TransUT1R(Interv_pred_range[input['choose_mjd'][0]].values.tolist(),
                              Interv_pred_range['UT1-UTC'].values.tolist(), input['tabl_tides'], input['tabl_tai'],
                              input['mod_UTC_UT1R']).type_transform()

    Pred_eop = pd.DataFrame(
        {input['choose_mjd'][0]: list(range(Start_pred_range, Start_pred_range + Max_length_pr, 1))})

    # Прогноз
    for i in input['choose_eop']:

        Pred_eop[i] = Local_aproximation.LAmethod(
            Interv_pred_range[i].to_list(),
            input['P_for_matrix_delay'], input['Tau'], input['Method'],
            Max_length_pr).methods_prediction(input['Rank_LA'])

        if 'UT1-UTC' in i:
            Pred_eop[i] = \
                UTC_TAI.TransUT1R(Pred_eop[input['choose_mjd'][0]].values.tolist(),
                                  Pred_eop[i].values.tolist(), input['tabl_tides'], input['tabl_tai'],
                                  input['mod_UTC_UT1R']).type_transform()

    return Pred_eop


def LA_repl_ref_iaa(input, Name_pred_range, Start_pred_range, Max_length_pr):
    from predictionEOP import Read_files, UTC_TAI
    from predictionEOP.mod_LA import Local_aproximation
    import scipy.interpolate as sc
    import pandas as pd

    # Чтение основного ряда
    Interv_pred_range = Read_files.ReadFile(Name_pred_range, str(Start_pred_range - input['Interval_for_prediction']),
                                            str(Start_pred_range - 1 - input['Interval_for_rapid_prediction']),
                                            input['choose_eop'],
                                            input['choose_mjd']).type_of_file()[
        input['choose_mjd'] + input['choose_eop']]

    ### для ипашных сырых

    IAA_range = Read_files.ReadFile(input['Name_iaa_range'],
                                    str(Start_pred_range - input['Interval_for_rapid_prediction'] - 2),
                                    str(Start_pred_range - 1), input['choose_eop'], input['choose_mjd']).type_of_file()

    IAA_range = IAA_range.reset_index()

    Inerp_eop = pd.DataFrame(
        {input['choose_mjd'][0]: list(
            range(Start_pred_range - input['Interval_for_rapid_prediction'], Start_pred_range, 1))})

    Inerp_eop[input['choose_eop'][0]] = sc.interp1d(IAA_range[input['choose_mjd'][0]], IAA_range[input['choose_eop'][0]], kind='slinear')(
        Inerp_eop[input['choose_mjd'][0]])

    # Тренд
    # Inerp_eop[input['choose_eop']] -= 0.000012

    Interv_pred_range = pd.concat([Interv_pred_range, Inerp_eop], ignore_index=True)


    # Перевод UT1-UTC в UT1-TAI
    if 'UT1-UTC' in input['choose_eop']:
        Interv_pred_range['UT1-UTC'] = \
            UTC_TAI.TransUT1R(Interv_pred_range[input['choose_mjd'][0]].values.tolist(),
                              Interv_pred_range['UT1-UTC'].values.tolist(), input['tabl_tides'], input['tabl_tai'],
                              input['mod_UTC_UT1R']).type_transform()

    Pred_eop = pd.DataFrame(
        {input['choose_mjd'][0]: list(range(Start_pred_range, Start_pred_range + Max_length_pr, 1))})

    # Прогноз
    for i in ['x_pole', 'y_pole', 'UT1-UTC']:
        if i in input['choose_eop']:
            Pred_eop[i] = Local_aproximation.LAmethod(
                Interv_pred_range[i].to_list(),
                input['P_for_matrix_delay'], input['Tau'], input['Method'],
                Max_length_pr).methods_prediction(input['Rank_LA'])

            if 'UT1-UTC' in i:
                Pred_eop[i] = \
                    UTC_TAI.TransUT1R(Pred_eop[input['choose_mjd'][0]].values.tolist(),
                                      Pred_eop[i].values.tolist(), input['tabl_tides'], input['tabl_tai'],
                                      input['mod_UTC_UT1R']).type_transform()

    return Pred_eop


def LA_repl_pred_iaa(input, Name_pred_range, Start_pred_range, Max_length_pr):
    from predictionEOP import Read_files, UTC_TAI
    from predictionEOP.mod_LA import Local_aproximation
    from scipy.interpolate import interpolate
    import pandas as pd

    # Чтение основного ряда
    Interv_pred_range = Read_files.ReadFile(Name_pred_range, str(Start_pred_range - input['Interval_for_prediction']),
                                            str(Start_pred_range - 1), input['choose_eop'],
                                            input['choose_mjd']).type_of_file()[
        input['choose_mjd'] + input['choose_eop']]

    # Перевод UT1-UTC в UT1-TAI
    if 'UT1-UTC' in input['choose_eop']:
        Interv_pred_range['UT1-UTC'] = \
            UTC_TAI.TransUT1R(Interv_pred_range[input['choose_mjd'][0]].values.tolist(),
                              Interv_pred_range['UT1-UTC'].values.tolist(), input['tabl_tides'], input['tabl_tai'],
                              input['mod_UTC_UT1R']).type_transform()

    Pred_eop = pd.DataFrame(
        {input['choose_mjd'][0]: list(range(Start_pred_range, Start_pred_range + Max_length_pr, 1))})

    # Прогноз
    for i in ['x_pole', 'y_pole', 'UT1-UTC']:
        if i in input['choose_eop']:
            Pred_eop[i] = Local_aproximation.LAmethod(
                Interv_pred_range[i].to_list(),
                input['P_for_matrix_delay'], input['Tau'], input['Method'],
                Max_length_pr).methods_prediction(input['Rank_LA'])

            if 'UT1-UTC' in i:
                Pred_eop[i] = \
                    UTC_TAI.TransUT1R(Pred_eop[input['choose_mjd'][0]].values.tolist(),
                                      Pred_eop[i].values.tolist(), input['tabl_tides'], input['tabl_tai'],
                                      input['mod_UTC_UT1R']).type_transform()

    ### для ипашных сырых
    IAA_range = Read_files.ReadFile(input['Name_iaa_range'],
                                    str(Start_pred_range - 2),
                                    str(Start_pred_range + input['Interval_for_replace']), input['choose_eop'],
                                    input['choose_mjd']).type_of_file()

    Inerp_eop = pd.DataFrame(
        {input['choose_mjd'][0]: list(
            range(Start_pred_range, Start_pred_range + input['Interval_for_replace'], 1))})
    Inerp_eop[input['choose_eop']] = interpolate.interp1d(IAA_range[input['choose_mjd'][0]],
                                                          IAA_range[input['choose_eop'][0]],
                                                          kind='slinear')(Inerp_eop[input['choose_mjd'][0]])

    Pred_eop.iloc[0:input['Interval_for_replace']] = Inerp_eop
    return Pred_eop


def LA_comb(config, Name_pred_range, Start_pred_range, Max_length_pr, choose_eop, choose_mjd):
    from predictionEOP import Read_files, UTC_TAI
    from predictionEOP.mod_LA import Local_aproximation
    from Determine_P_for_matrix import DetermineP

    Interval_for_prediction = 365 * int(config["final_range"]["interval_for_prediction"])
    Method = config["pred_range"]["method"]
    Rank_LA = int(config["pred_range"]["rank"])
    Tau = int(config["pred_range"]["tau"])
    tabl_tides = (config["final_range"]["tabl_tides"])
    tabl_tai = (config["final_range"]["tabl_tai"])
    mod_UTC_UT1R = (config["final_range"]["mod_utc"])

    # Чтение основного ряда
    Interv_pred_range = Read_files.ReadFile(Name_pred_range, str(Start_pred_range - Interval_for_prediction),
                                            str(Start_pred_range + Max_length_pr - 1), choose_eop,
                                            choose_mjd).type_of_file()

    # Перевод UT1-UTC в UT1-TAI
    if 'UT1-UTC' in choose_eop:
        Interv_pred_range.loc[0:Interval_for_prediction - 1, 'UT1-UTC'] = \
            UTC_TAI.TransUT1R(Interv_pred_range[choose_mjd[0]][0:Interval_for_prediction].values.tolist(),
                              Interv_pred_range['UT1-UTC'][0:Interval_for_prediction].values.tolist(), tabl_tides,
                              tabl_tai,
                              mod_UTC_UT1R).type_transform()

    length_short = 5
    # Задание параметра Р
    if (config["pred_range"]["flag_p"]) == 'yes':
        P_for_short = DetermineP(Interv_pred_range[choose_eop[0]], Interval_for_prediction, 90, Tau).fnn()
        P_for_long = DetermineP(Interv_pred_range[choose_eop[0]], Interval_for_prediction, 1000, Tau).fnn()
    else:
        P_for_short = int(config["pred_range"]["p_short"])
        P_for_long = int(config["pred_range"]["p_long"])
    print('P_for_matrix_delay   ', P_for_short)

    # Прогноз
    for i in ['x_pole', 'y_pole', 'UT1-UTC']:
        if i in choose_eop:
            Interv_pred_range.loc[Interval_for_prediction::, i] = Local_aproximation.LAmethod(
                Interv_pred_range[i][0:Interval_for_prediction].to_list(),
                P_for_long, Tau, Method,
                Max_length_pr).methods_prediction(Rank_LA)
            Interv_pred_range.loc[Interval_for_prediction:Interval_for_prediction + length_short, i] = \
                Local_aproximation.LAmethod(
                    Interv_pred_range[i][0:Interval_for_prediction].to_list(),
                    P_for_short, Tau, Method, length_short).methods_prediction(Rank_LA)
            if 'UT1-UTC' in i:
                Interv_pred_range.loc[:, i] = \
                    UTC_TAI.TransUT1R(Interv_pred_range[choose_mjd[0]].values.tolist(),
                                      Interv_pred_range[i].values.tolist(), tabl_tides, tabl_tai,
                                      mod_UTC_UT1R).type_transform()
        else:
            Interv_pred_range.loc[Interval_for_prediction::, i] = 0

    return Interv_pred_range[Interval_for_prediction::]
