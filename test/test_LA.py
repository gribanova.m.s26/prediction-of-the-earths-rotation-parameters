import configparser

import pytest
from predictionEOP import Method_prediction, Read_files, UTC_TAI


def test_ReadFile():
    finals = Read_files.ReadFile('C:\\Users\\User\\PycharmProjects\\LocalAproximation\\docs\\finals.data',
                      '57488', '57498', ['x_pole'], ['MJD']).type_of_file()
    finals_f = 0.003327
    finals_l = 0.018550

    assert finals['x_pole'][0] == finals_f
    assert finals['x_pole'][10] == finals_l

    eopc = Read_files.ReadFile('C:/Users/User/PycharmProjects/LocalAproximation/docs/eopc04_14_IAU2000.62-now',
                    '57488', '57498', ['x_pole'], ['MJD']).type_of_file()
    eopc_f = 0.003303
    eopc_l = 0.018493

    assert eopc['x_pole'][0] == eopc_f
    assert eopc['x_pole'][10] == eopc_l

    bull = Read_files.ReadFile('C:/Users/User/PycharmProjects/LocalAproximation/docs/volume-xxxiii/bulletina-xxxiii-001.txt',
                    '58851', '58861', ['x_pole'], ['MJD']).type_of_file()

    bull_f = 0.0730
    bull_l = 0.0583

    assert bull['x_pole'][0] == bull_f
    assert bull['x_pole'][10] == bull_l


def test_UTC():
    tabl_tides = 'C:/Users/User/PycharmProjects/LocalAproximation/EARTH ROTATION VARIATIONS DUE TO ZONAL TIDES.txt'
    tabl_tai = 'C:/Users/User/PycharmProjects/LocalAproximation/TAI_UTC.DAT'
    mod_UTC_UT1R = 'no'
    mjd_range = [57488, 57489]
    UTC_range = [-0.1017106, -0.1036653]

    Mod_utc = \
        UTC_TAI.TransUT1R(mjd_range, UTC_range, tabl_tides, tabl_tai,
                  mod_UTC_UT1R).type_transform()

    assert Mod_utc == [36.1017106, 36.1036653]

# def test_LA():
