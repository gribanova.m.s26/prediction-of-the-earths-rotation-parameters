from setuptools import setup, find_packages

setup(name='predictionEOP',

      url='https://gitlab.com/gribanova.m.s26/prediction-of-the-earths-rotation-parameters',

      author='Gribanova Marina',

      author_email='gribanova.m.s26@gmail.com',

      description='Prediction of the Earths rotation parameters using several methods',

      packages=find_packages(exclude=['tests']),

      #long_description=open('README.md').read(),

      zip_safe=False,

      )
