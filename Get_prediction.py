from predictionEOP import Read_config, Method_prediction
import time


start_time = time.time()

# Получение параметров из файла
Input = Read_config.read_config(Path='C:/Users/User/PycharmProjects/LocalAproximation/')

# Получение прогноза
Pred_eop = Method_prediction.choose_method(Input, Input['Name_pred_range'], Input['Start_mjd'], Input['Max_length_pr'])


# Запись в файл
with open(Input['tabl_pred'] + str(Input['Start']) + '_' + str(Input['mod_get_pred']) + '.txt', "w") as my_file:
    Pred_eop.to_csv(my_file, index=False)


print(time.time() - start_time)



#if __name__ == "__main__":
#    import Read_config
#    from astropy.time import Time
#    import dateutil.parser
#
#    # Получение параметров из файла
#    Input = Read_config.read_config(Path='C:/Users/User/PycharmProjects/LocalAproximation/')
#    Start_pred_range = int((Time(dateutil.parser.parse(Input['Start']))).mjd)
#
#
#    # Получение прогноза
#    Pred_eop = get_prediction_w_repl_iaarapid(Input,Input['Name_pred_range'], Start_pred_range, Input['Max_length_pr'])
#
#    # Запись в файл
#    with open(Input['tabl_pred'] + str(Input['Start']) + '.txt', "w") as my_file:
#        Pred_eop.to_csv(my_file, index=False)
