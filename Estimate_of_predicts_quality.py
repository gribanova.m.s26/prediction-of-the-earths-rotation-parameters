# Программа для оценки качества прогноза с помощью СКО и апробации
# цикл для подсчета разностей реального и спрогнозированного ряда с шагом тестирования прогноза
# (определяет количество дней, после которых берётся новая дата начала прогноза) и вычисления СКО на их основе
import os
import numpy as np
import time
import matplotlib.pyplot as plt
from astropy.time import Time
import pandas as pd
from predictionEOP import Read_config, Method_prediction, Read_files


start_time = time.time()

'''''''''''''''''''''''# Выбор начальной и конечной даты (из файлов для апробации)
Date_aprob = {'2015': ['2015-01-02', '2015-12-25'],
              '2016': ['2016-01-01', '2016-12-30'],
              '2017': ['2017-01-06', '2017-12-29'],
              '2018': ['2018-01-05', '2018-12-28'],
              '2019': ['2019-01-04', '2019-12-27'],
              , }'''''''''''''''''''''''


# Разница реального и спрогнозированного ряда
def difference(real_var, pred_var, length):  # разности
    diff = np.zeros(length)
    for i in range(len(pred_var)):
        diff[i] = (real_var.to_list()[i] - pred_var.to_list()[i]) * 1000
    return diff


# Подсчет РМС
def rms(diff, length, ):  # СКО
    mae_var = np.zeros(length)
    rms_var = np.zeros(length)
    max_var = np.zeros(length)
    numb_pred = np.zeros(length)
    for i in range(length):
        numb_pred[i] = len([i for i in diff[:, i] if i != 0])
        if numb_pred[i] == 0:
            numb_pred[i] = 1
        rms_var[i] = np.sqrt((np.sum(diff[:, i] ** 2)) / numb_pred[i])
        mae_var[i] = (np.sum(np.abs(diff[:, i]))) / numb_pred[i]
    return rms_var, mae_var


# Получение параметров из файла
Input = Read_config.read_config(Path='C:/Users/User/PycharmProjects/LocalAproximation/')

fig, ax1 = plt.subplots()
ax1.grid(which='major', color='grey')
ax1.minorticks_on()
ax1.grid(which='minor', color='silver', linestyle=':')

# несколько файлов или один
if Input['Name_pred_range'][-1] == '/':
    List_pred_range = os.listdir(Input['Name_pred_range'])
else:
    List_pred_range = []

Year = Input['Start'][0:4]

# Start_final_range = int((Time(dateutil.parser.parse(Input['Start']))).mjd)
# End_final_range = int((Time(dateutil.parser.parse(Input['End']))).mjd)

Start_final_range = Input['Start_mjd']
End_final_range = Input['End_mjd']

# чтения финального ряда для сравнения
Final_EOP = Read_files.ReadFile(Input['Name_final_range'], (Start_final_range),
                                (End_final_range - 1), Input['choose_eop'], Input['choose_mjd']).type_of_file()


# Цикл подсчета разностей
diff_eop = np.zeros(
    (len(Input['choose_eop']), (End_final_range - Start_final_range) // Input['Step_test'], Input['Max_length_pr']))

for i in range((End_final_range - Start_final_range) // Input['Step_test']):
    print(i)
    Start_pred_range = Start_final_range + Input['Step_test'] * i
    End_pred_range = Start_pred_range + Input['Max_length_pr']
    if End_pred_range > End_final_range:
        End_pred_range = End_final_range

    # имя файла для прогноза
    if List_pred_range:
        date_str = ''.join([i for i in Time(Start_pred_range, format='mjd').fits if i != '-'])[0:8]
        print(date_str)
        try:
            Pred_file = Input['Name_pred_range'] + [i for i in List_pred_range if date_str in i][0]
        except:
            continue
    else:
        Pred_file = Input['Name_pred_range']


    # чтение ряда для прогноза свой/чужой
    if Input['flag_pred'] == 'other':
        Pred_EOP = Read_files.ReadFile(Pred_file, (Start_pred_range),
                                       (End_pred_range - 1), Input['choose_eop'], Input['choose_mjd']).type_of_file()
    else:
        Pred_EOP = Method_prediction.choose_method(Input,
                                         Pred_file, Start_pred_range, (End_pred_range - Start_pred_range), )


    print('Pred ', Pred_EOP)
    print('Final ', Final_EOP[i * Input['Step_test']: End_pred_range - Start_final_range])

    for k, var in enumerate(Input['choose_eop']):  # подсчет разностей
        diff_eop[k, i, :] = difference(Final_EOP[var][
                                       i * Input['Step_test']: End_pred_range - Start_final_range],
                                       Pred_EOP[var], Input['Max_length_pr'])
    ax1.plot(Final_EOP[Input['choose_mjd']][Input['Step_test'] * i: (End_pred_range - Start_final_range)],
             Pred_EOP[Input['choose_eop'][0]] * 1000,
             color='black', linewidth=0.9)

# график
ax1.set_xlabel(Input['choose_mjd'][0], fontsize=26)
if Input['choose_eop'][0] == 'UT1-UTC':
    ax1.set_ylabel("ms", fontsize=26)
else:
    ax1.set_ylabel("мс дуги", fontsize=26)
ax1.tick_params(axis='both', labelsize=20)
ax1.plot(Final_EOP[Input['choose_mjd'][0]], Final_EOP[Input['choose_eop'][0]] * 1000
         , color='red', label='final row', linewidth=1)
# ax1.set_title('Ряд ' + Input['choose_eop'][0] + ' за ' + str(Year) + ' год', fontsize=26)
plt.legend()
plt.show()

# Статистика
Tabl_rms = pd.DataFrame()
Tabl_rms['Days'] = range(1, Input['Max_length_pr'] + 1)
for k, var in enumerate(Input['choose_eop']):
    Tabl_rms['rms_' + var],  Tabl_rms['mae_' + var] = rms(diff_eop[k, :, :],Input['Max_length_pr'], )

output_name = Input['tabl_rms'] + '_' + str(Input['Start']) + '_' + str(Input['Method']) + '.txt'


# Запись в файл
def output(name_file, name_tabl):
    with open(name_file, "a") as my_file:
        my_file.write(Input['Start'] + ' --- ' + Input['End'] + ' ; ')
        my_file.write(f"{Input['mod_get_pred']}" + ' ; ')
        my_file.write(f"{Input['Method']}" + ' ; ')
        my_file.write(f"I = {Input['Interval_for_prediction']} P =  {Input['P_for_matrix_delay']}" + '\n')
        #my_file.write(f"{Input['Interval_for_rapid_prediction']}" + '\n')
    name_tabl.to_csv(name_file, index=False, header=True, decimal='.', sep='\t', float_format='%.5f', mode='a')


output(output_name,
       Tabl_rms.loc[[x for x in [0, 1, 2, 3, 4, 9, 14, 19, 39, 89] if x <= Input['Max_length_pr']]])
