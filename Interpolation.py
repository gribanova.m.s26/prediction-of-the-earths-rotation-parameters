import pandas as pd
import scipy.interpolate as sc


def interpol(start=59580, end=59950, name_file='input/iaa_r_R3.eop'):
    numb_col = {'MJD': 0, 'x_pole': 1, 'y_pole': 2, 'UT1-UTC': 3}
    df = (pd.read_table(name_file, sep='\s+', header=None))[[numb_col[i] for i in (['MJD'] + ['UT1-UTC'])]]
    df.columns = ['MJD'] + ['UT1-UTC']
    df = df[((int(start) - 2 <= df['MJD']) & (df['MJD'] <= int(end) + 1))]
    Inerp_eop = pd.DataFrame({'MJD': list(range(start, end, 1))})
    Inerp_eop['UT1-UTC'] = sc.interp1d(df['MJD'], df['UT1-UTC'], kind='slinear')(
        Inerp_eop['MJD'])
    #Inerp_eop.to_csv(name_file + '.csv', index=False)
    return Inerp_eop


print(interpol())
